# Library A

## Description
This library has a method that performs a sum given two inputs.

## Installation
You can pull this library into your project using Peru.

This library was developed using LabVIEW 2021 Community Edition.

## Usage
With two numeric inputs, this library gives you a sum of them.

## Authors and acknowledgment
- Felipe Pinheiro Silva

## License
MIT
