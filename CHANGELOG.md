# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
### [2.0.1](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-a/compare/v2.0.0...v2.0.1) (2022-02-16)

## [2.0.0](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-a/compare/v1.0.1...v2.0.0) (2022-02-16)


### ⚠ BREAKING CHANGES

* this commit breaks compatibility with current projects using this library, because of the change in connector pane.

### Bug Fixes

* changes conector pane order ([e76eaa6](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-a/commit/e76eaa6ed968ae3a6a4e44a8466fcf6d0d4cfc9d))

### [1.0.1](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-a/compare/v1.0.0...v1.0.1) (2022-02-13)

## 1.0.0 (2022-02-13)


### Features

* adds sum function ([35a9889](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-a/commit/35a9889fc60575c511b83d1c43c82967f9b97a08))


### Docs

* adds readme file ([315e361](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-a/commit/315e3614bfa56d3dc5ef09ed1f90fe392397bd21))


### CI

* adds automatic versioning config file ([d0f12aa](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/lib-a/commit/d0f12aa584419bc868fffd2af41129ae500b3b90))
